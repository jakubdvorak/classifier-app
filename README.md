# Classifier App
This is the simple Flask app that predicts value based on 3 provided features. It's a part of task for SNQ

### Notes
Code is written in <code>Python 3.7.3</code>

### Requiremements
- <code>Flask==1.1.1</code>
- <code>Flask-Migrate==2.5.2</code>
- <code>Flask-SQLAlchemy==2.4.1</code>
- <code>SQLAlchemy==1.3.5</code>
- <code>pandas==0.24.2</code>
- <code>ijson==2.5.1</code>
- <code>scikit-learn==0.21.2</code>
- <code>pickle-mixin==1.0.2</code>

### Installation
1. Run <code>flask db init</code>
2. Run <code>flask db migrate</code>
3. Run <code>flask db upgrade</code>
4. Generate *prediction model* with <code>python mlmodel.py</code>

### Usage
Start app with  <code>python app.py</code>

#### Endpoints
##### classify
The first endpoint  `/classify`  accepts  `GET`  requests with three parameters  `f1`,  `f2`, and  `f3`  (the features used in the training data). For example:
https://localhost:5000/classify?f3=A&f1=0.23&f2=0.4

##### stats
The second enpoint  `/stats`  accepts  `GET`  requests with no parameters:
https://localhost:5000/stats

The response contains statistics for the logged classification requests with  `status=OK`: mean values for  `f1`,  `f2`, and the most frequent value for  `f3`.
