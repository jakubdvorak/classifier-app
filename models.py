from app import db

class ClassificationRequest(db.Model):
    id_request = db.Column(db.Integer, primary_key=True)
    request_timestamp = db.Column(db.String(64), index=True, unique=True)
    predicted_class = db.Column(db.String(120))
    response_status = db.Column(db.String(128))
    error_message = db.Column(db.String(128))

    def __repr__(self):
        return '<ClassificationRequest {}>'.format(self.id_request)

class ClassificationRequestParam(db.Model):
    id_request_param= db.Column(db.Integer, primary_key=True)
    id_request = db.Column(db.Integer, index=True)
    param_name = db.Column(db.String(64))
    param_value = db.Column(db.String(120))

    def __repr__(self):
        return '<ClassificationRequestParam {}>'.format(self.id_request_param)
