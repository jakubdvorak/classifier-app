from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func, desc, exc

from flask_migrate import Migrate

from config import Config

from sklearn.svm import SVC
import pickle
import json


from datetime import datetime



app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

import models

@app.route('/classify', methods = ['GET'])


def predict():
    '''
    This view loads prediction model, tests and validate existence of 3 required
    features and stores information in SQLite database.
    '''
    model = pickle.load(open('model.pkl','rb'))

    mappings = {'A': 1, 'B': 2, 'D': 3, 'E': 4 }

    response = dict()

    app.logger.info('Test existence of all arguments')

    if 'f1' in request.args and 'f2' in request.args and 'f3' in request.args:
        try:
            app.logger.info('Casting f1: {} and f2: {} to float'.format(request.args.get('f1'), request.args.get('f2')))
            f1 = float(request.args.get('f1'))
            f2 = float(request.args.get('f2'))
        except ValueError:
            response['status'] = 'ERROR'
            response['error_message'] = 'f1 or f2 are not a float'
            return jsonify(response)

        app.logger.info('Searching value {} in allowed mappings'.format(request.args.get('f3')))
        if request.args.get('f3') in mappings:

            f3 = mappings.get(request.args.get('f3'))
            try:
                X = [[f1, f2, f3]]
                prediction = model.predict(X)
                response['status'] = 'OK'
                response['predicted_class'] =  int(prediction[0])



                last_two_predictions = db.session.\
                                        query(models.ClassificationRequest.predicted_class).\
                                        order_by(models.ClassificationRequest.request_timestamp.desc()).limit(2)

                count = 0
                for record in last_two_predictions:
                    if int(record[0]) == int(prediction[0]):
                        count+=1

                if count == 2:
                    response['status'] = 'WARNING'

                timestamp = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S%z')

                db_request = models.ClassificationRequest(request_timestamp = f'{timestamp}',
                                                          predicted_class = str(prediction[0]),
                                                          response_status = response['status'],
                                                          error_message = '')


                db.session.add(db_request)
                db.session.flush()

                f1_db_request_param = models.ClassificationRequestParam(id_request = db_request.id_request,
                                                                        param_name = 'f1',
                                                                        param_value = f1)

                f2_db_request_param = models.ClassificationRequestParam(id_request = db_request.id_request,
                                                                        param_name = 'f2',
                                                                        param_value = f2)

                f3_db_request_param = models.ClassificationRequestParam(id_request = db_request.id_request,
                                                                        param_name = 'f3',
                                                                        param_value = request.args.get('f3'))

                db.session.add(f1_db_request_param)
                db.session.add(f2_db_request_param)
                db.session.add(f3_db_request_param)
                db.session.commit()
                return jsonify(response)

            except Exception as e:
                db.session.rollback()
                response['status'] = 'ERROR'
                response['error_message'] = str(e)
                return jsonify(response)

        else:
            response['status'] = 'ERROR'
            response['error_message'] = 'Provided f3 value is not in a list of allowed values. Allowed values are: A, B, D or E.'
            return jsonify(response)

    else:
        response['status'] = 'ERROR'
        response['error_message'] = 'Missing arguments'
        return jsonify(response)

@app.route('/stats', methods = ['GET'])

def get_stats():
    '''
    This view shows stats from previous predictions.
    '''
    response = dict()

    try:
        mean_f1 = db.session.query(func.avg(models.ClassificationRequestParam.param_value)).\
                 filter_by(param_name='f1')

        mean_f2 = db.session.query(func.avg(models.ClassificationRequestParam.param_value)).\
                 filter_by(param_name='f2')

        most_frequent_f3 = db.session.\
                            query(models.ClassificationRequestParam.param_value,
                                  func.count(models.ClassificationRequestParam.param_value).label('qty')).\
                            group_by(models.ClassificationRequestParam.param_value).\
                            order_by(desc('qty')).limit(1)

        response['mean_f1'] = float(mean_f1[0][0])
        response['mean_f2'] = float(mean_f2[0][0])
        response['most_frequent_f3'] = str(most_frequent_f3[1][0])
        response['status'] = 'OK'

    except Exception as e:
        response['status'] = 'ERROR'
        response['message'] = str(e)
        db.session.rollback()

    return jsonify(response)

if __name__ == '__main__':
    app.run(debug=True)
