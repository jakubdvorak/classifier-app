import pandas as pd

#from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.svm import SVC

import pickle

training_data = pd.read_csv('training_data.csv')

#I decided to drop rows where the feature f3 is missing as it represents less than 5% of all observations
training_data.dropna(inplace = True)

"""Feature f3 must be encoded as it represents categorical data.
   Number of features is small so I will encode them manualy"""

replace_map = {'f3': {'A': 1, 'B': 2, 'D': 3, 'E': 4 }}
training_data.replace(replace_map, inplace = True)

#features
X = training_data[['f1', 'f2', 'f3']]

#labels
y = training_data['target']

X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)
scaler = MinMaxScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

svm = SVC()
svm.fit(X_train, y_train)
print('Accuracy of SVM classifier on training set: {:.2f}'
     .format(svm.score(X_train, y_train)))
print('Accuracy of SVM classifier on test set: {:.2f}'
     .format(svm.score(X_test, y_test)))

#Exporting model using pickle
pickle.dump(svm, open('model.pkl','wb'))
